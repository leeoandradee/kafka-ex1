package com.mastertech.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AccessProducer {
    
    @Autowired
    private KafkaTemplate<String, Access> producer;

    public void sendToKafka(Access access) {
        producer.send("spec4-leonardo-andrade-1", access);
    }

}