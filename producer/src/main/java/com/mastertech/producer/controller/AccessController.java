package com.mastertech.producer.controller;

import com.mastertech.producer.Access;
import com.mastertech.producer.AccessProducer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccessController {

    @Autowired
    private AccessProducer accessProducer;

    @GetMapping("/{clientId}/{doorId}")
    public void sendAccess(@PathVariable(name = "clientId") int clientId, @PathVariable(name = "doorId") int doorId) {
        Access access = new Access();
        access.setClientId(clientId);
        access.setDoorId(doorId);
        access.setHasAccess(true);
        accessProducer.sendToKafka(access);
    }

    
}