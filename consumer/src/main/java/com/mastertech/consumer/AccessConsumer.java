package com.mastertech.consumer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.mastertech.producer.Access;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class AccessConsumer {

    @KafkaListener(topics = "spec4-leonardo-andrade-1", groupId = "group-1")
    public void receive(@Payload Access access) {
        System.out.println("Recieve the access");
        try {
            writeCsvFile(access);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeCsvFile(Access access) throws IOException {

        String filePath = "C:/Users/Leonardo/Documents/new.csv";
        File file = new File(filePath);

        if (!file.exists()) {
            FileWriter csvWriter = new FileWriter(filePath);
            csvWriter.append("Client ID");
            csvWriter.append(",");
            csvWriter.append("Door ID");
            csvWriter.append(",");
            csvWriter.append("Access");
            csvWriter.flush();
            csvWriter.close();
        }
        FileWriter csvWriter = new FileWriter(filePath);
        csvWriter.append("\n");
        csvWriter.append(String.valueOf(access.getClientId()));
        csvWriter.append(",");
        csvWriter.append(String.valueOf(access.getDoorId()));
        csvWriter.append(",");
        csvWriter.append(String.valueOf(access.isHasAccess()));
        csvWriter.flush();
        csvWriter.close();

    }

}